package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ro.dataProvider.ConfigFileReader;
import ro.dataProvider.TestBase;

public class LoginPage extends TestBase {

    public LoginPage(WebDriver webDriver, ConfigFileReader configFileReader) {
        super ( webDriver, configFileReader );
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement ( by );
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }


}
