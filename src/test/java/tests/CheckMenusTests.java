
package tests;


import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ro.dataProvider.DefaultLogin;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CheckMenusTests {

    private By liElements = By.xpath ( "//ul[@class='toggle-footer']" );

    @Test
    public void checkMenus() {
        DefaultLogin instance = new DefaultLogin ();
        instance.login ();

        WebDriver driver = instance.getDriver ();

        // collect all elements as Web elements from the site
        WebElement ul = driver.findElement ( liElements );
        List<WebElement> actualWebList = ul.findElements ( By.tagName ( "li" ) );

        // convert the List of web elements in a list of Strings
        List<String> actualList = new ArrayList<String> ();

        for (WebElement e : actualWebList) {
            actualList.add ( e.getText () );
        }

        // declare the expected list elements
        List<String> expectedList = new LinkedList<> ();
        expectedList.add ( "Specials" );
        expectedList.add ( "New products" );
        expectedList.add ( "Best sellers" );
        expectedList.add ( "Our stores" );
        expectedList.add ( "Contact us" );
        expectedList.add ( "Terms and conditions of use" );
        expectedList.add ( "Sitemap" );

        //alternative I
        boolean areEqual =  actualList.containsAll ( expectedList );
        if (areEqual){
            System.out.println ( "Lists are not equal." );
        } else {
            System.out.println ( "Lists are not equal." );
        }


        // alternative II -> compare both lists
        for (int i = 0; i < expectedList.size (); i++) {

            if (actualList.contains ( expectedList.get ( i ) )) {
                System.out.println ( "The menu item is listed correct =>   " + expectedList.get ( i ) );
            } else {
                System.out.println ( "The menu item is <not> listed correct =>   " + expectedList.get ( i ) );
            }
        }

        driver.quit ();
    }

}






