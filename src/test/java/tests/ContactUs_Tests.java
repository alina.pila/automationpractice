package tests;


import ro.dataProvider.DefaultLogin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ContactUs_Tests {

    private By contactLinkElement = By.id("contact-link");
    private By subjectHeadingElement = By.id("id_contact");
    private By emailElement = By.cssSelector("[name='from']");
    private By fileUploadElement = By.id("fileUpload");
    private By sendUploadElement = By.id("submitMessage");
    private By messageUploadElement = By.id("message");
    private By notificationUploadElement= By.cssSelector(".alert-success");

    public ContactUs_Tests() {

    }

    @Before
    public void beforeTest() {
        System.out.println("Before test is called.");
    }

    @Test
    public void validForm() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement contactUsLink = driver.findElement(contactLinkElement);
        contactUsLink.click();
        System.out.println("Contact us section is selected.");

        WebElement subjectLinkDropdown = driver.findElement(subjectHeadingElement);
        Select selectDropdown = new Select(subjectLinkDropdown);
        selectDropdown.selectByIndex(2);
        System.out.println("Dropdown 'Subject Heading' is selected.");

        WebElement emailContact = driver.findElement(emailElement);
        emailContact.clear();
        emailContact.sendKeys("testContact@yahoo.com");

        System.out.println("E-mail is entered.");

        WebElement fileUpload = driver.findElement(fileUploadElement);
        //Linux path: fileUpload.sendKeys("C:\\Users\\a.pila\\Documents\\workspace-sts\\AutomatedTest\\fileUpload.txt");
        fileUpload.sendKeys("C:\\Users\\a.pila\\Documents\\workspace-sts\\AutomatedTest\\fileUpload.txt");
        System.out.println("File is uploaded.");

        WebElement messageUpload = driver.findElement(messageUploadElement);
        messageUpload.sendKeys("Message is written.");
        System.out.println("Upload message is written.");

        WebElement sendUpload = driver.findElement(sendUploadElement);
        sendUpload.click();
        System.out.println("Send button is clicked.");

        String expectedUploadMessage = "Your message has been successfully sent to our team.";
        String messageUploadInstance = instance.getDriver().findElement(notificationUploadElement).getText();
        Assert.assertTrue("Your message has been successfully sent to our team.", messageUploadInstance.contains(expectedUploadMessage));
        System.out.println("Notification message is present on page.");

        driver.quit();
    }

}



