package ro.dataProvider;

import java.io.*;
import java.util.Properties;

public class ConfigFileReader {

	private Properties properties = new Properties();
	//private static  final String propertyFilePath= "C:\\Users\\a.pila\\Documents\\workspace-sts\\AutomatedTest\\src\\main\\resources\\configuration.properties";
	/*private static final String propertyFilePath ="/home/pillblast/IdeaProjects/automationpractice/src/main/resources" +
			"/configuration.properties";*/

	public ConfigFileReader() {
		InputStream configFile = getClass().getClassLoader().getResourceAsStream("configuration.properties");

		try {
			properties.load(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (configFile !=null){
				try {
					configFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String getDriverPath(){
		String driverPath = properties.getProperty("driverPath");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");
	}

	public long getImplicitlyWait() {
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
		else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");
	}

	public String getApplicationUrl() {
		String url = properties.getProperty("url");
		if(url != null) return url;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}

	public String getValidUsername() {
		String validusername = properties.getProperty("validUsername");
		if(validusername != null) return validusername;
		else throw new RuntimeException("Valid username is NOT specified in the Configuration.properties file.");
	}

	public String getValidPassword() {
		String validpassword = properties.getProperty("validPassword");
		if(validpassword != null) return validpassword;
		else throw new RuntimeException("Valid password is NOT specified in the Configuration.properties file.");
	}

	public String getInvalidUsername() {
		String invalidusername = properties.getProperty("invalidUsername");
		if(invalidusername != null) return invalidusername;
		else throw new RuntimeException("Invalid username is NOT specified in the Configuration.properties file.");
	}

	public String getInvalidPassword() {
		String invalidpassword = properties.getProperty("invalidPassword");
		if(invalidpassword != null) return invalidpassword;
		else throw new RuntimeException("Invalid password is NOT specified in the Configuration.properties file.");
	}
}