package ro.dataProvider;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DefaultLogin {

    private ConfigFileReader configFileReader = new ConfigFileReader();
    private By usernameBy = By.id("email");
    private By passwordBy = By.cssSelector("input[name=passwd]");
    private By clickBy = By.xpath("//button[@id='SubmitLogin']/span");

    @Test
    public void login () {
        LoginFactory instance = LoginFactory.getInstance();
        instance.openAndMaximizeWindow(configFileReader);
        instance.setValidUsername(configFileReader.getValidUsername(), usernameBy);
        instance.setValidPassword(configFileReader.getValidPassword(), passwordBy);
        instance.clickSubmit(clickBy);
        System.out.println("User is logged in.");
    }

    public WebDriver getDriver() {
        return LoginFactory.getInstance().getDriver();
    }
}
